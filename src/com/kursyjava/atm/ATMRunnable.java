package com.kursyjava.atm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Map.Entry;
import java.util.SortedMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ATMRunnable implements Runnable {
    private Socket clientSocket;
    private SortedMap<String, CurrencyBox> currencyBoxes;

    public ATMRunnable(Socket clientSocket, SortedMap<String, CurrencyBox> currencyBoxes) {
        this.clientSocket = clientSocket;
        this.currencyBoxes = currencyBoxes;
    }

    @Override
    public void run() {
        try {
            Charset charset = Charset.forName("UTF8");
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), charset));

            String inputLine = "";
            out.println("\r\n\n");
            boolean isAuthorized = false;

            while ((inputLine = in.readLine()) != null) {
                // Hexa value of BOM = EF BB BF => int 65279
                inputLine = inputLine.replaceAll("[\uFEFF-\uFFFF]", "");
                // баг иза UTF-8 BOM стандартными способами решить не смог
                System.out.println(inputLine);
                if (!isAuthorized) {
                    // -l <login> -p <password>
                    // check
                    // exit or isAuthorise = true
                    // word[0] = "-l"

                    // word[1] != null lenght
                    // word[2] = "-p"
                    // word[3] != null lenght

                    isAuthorized = ATMServices.isAuthorized(out, inputLine);
                    if (!isAuthorized) {
                        clientSocket.close();
                        break;
                    }
                }

                if (ATMServices.isAdd(inputLine)) {

                    ATMServices.add(inputLine, currencyBoxes);
                    out.println("OK");

                } else if (ATMServices.isWithdraw(inputLine, currencyBoxes)) {

                    ATMServices.withdraw(inputLine, out, currencyBoxes);
                    out.println("OK");

                } else if (inputLine.equals("?")) {

                    for (CurrencyBox currencyBox : currencyBoxes.values()) {
                        for (Entry<Integer, Integer> each : currencyBox.getMoneys()) {
                            out.println(currencyBox.getCurrency() + " " + each.getKey() + " " + each.getValue());
                        }
                    }
                    out.println("OK");

                } else if (inputLine.equals("EXIT")) {
                    out.println("BYE");
                    clientSocket.close();
                    break;
                } else {
                    out.println("ERROR");
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }

}
