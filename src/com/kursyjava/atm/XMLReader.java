package com.kursyjava.atm;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLReader {

    public static int getPort() throws ParserConfigurationException, SAXException, IOException {
        File file = new File("settings.xml");

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        document.getDocumentElement().normalize();
        String portText = document.getElementsByTagName("port").item(0).getTextContent();
        int port = Integer.valueOf(portText);

        return port;
    }

    public static Map<String, String> getUsersMap() throws ParserConfigurationException, SAXException, IOException {
        Map<String, String> hashmap = new HashMap<>();
        File file = new File("settings.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        document.getDocumentElement().normalize();
        NodeList nodesList = document.getElementsByTagName("user");

        for (int i = 0; i < nodesList.getLength(); i++) {

            Node node = nodesList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String username = element.getElementsByTagName("username").item(0).getTextContent();
                String password = element.getElementsByTagName("password").item(0).getTextContent();
                hashmap.put(username, password);
            }

        }
        return hashmap;
    }

}
