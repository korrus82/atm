package com.kursyjava.atm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ATMServer {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        int port = XMLReader.getPort();
        ServerSocket serverSocket = new ServerSocket(port);
        SortedMap<String, CurrencyBox> currencyBoxes = new TreeMap<>();

        try {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                Thread thread = new Thread(new ATMRunnable(clientSocket, currencyBoxes));
                thread.start();
            }
        } finally {
            serverSocket.close();
        }
    }

}
