package com.kursyjava.atm;

import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class CurrencyBox {
    private final String currency;
    // key nominal value count
    private final SortedMap<Integer, Integer> moneyMap;

    public CurrencyBox(String currency) {
        this.currency = currency;
        this.moneyMap = new TreeMap<>();
    }

    public String getCurrency() {
        return currency;
    }

    public Set<Entry<Integer, Integer>> getMoneys() {
        return moneyMap.entrySet();
    }

    public int getNominalCount(int nominal) {
        return moneyMap.getOrDefault(nominal, 0);
    }

    public void addMoney(int nominal, int count) {
        moneyMap.put(nominal, count + getNominalCount(nominal));
    }

    public void removeMoney(int nominal) {
        int newCount = getNominalCount(nominal) - 1;
        if (newCount > 0) {
            moneyMap.put(nominal, newCount);
        } else {
            moneyMap.remove(nominal);
        }
    }

    @Override
    public String toString() {
        return "CurrencyBox [currency=" + currency + ", moneyMap=" + moneyMap + "]";
    }

    // public static void main(String[] args) throws IOException {
    // // + USD 100 5
    // Map<String, CurrencyBox> currencyBoxes = new HashMap<>();
    // currencyBoxes.put("USD", new CurrencyBox("USD"));
    // currencyBoxes.put("RUR", new CurrencyBox("RUR"));
    // currencyBoxes.get("USD").addMoney(50, 500);
    // currencyBoxes.get("RUR").addMoney(50, 500);
    // currencyBoxes.get("USD").addMoney(100, 50);
    // currencyBoxes.get("RUR").addMoney(100, 50);
    // currencyBoxes.get("USD").addMoney(500, 5);
    // currencyBoxes.get("RUR").addMoney(500, 5);
    // System.out.println(currencyBoxes);
    // for (CurrencyBox currencyBox : currencyBoxes.values()) {
    // for (Entry<Integer, Integer> each : currencyBox.getMoneys()) {
    // System.out.println(currencyBox.getCurrency() + " " + each.getKey() + " " + each.getValue());
    // }
    // }
    // int copyAmount = 3200;
    // String currencyCode = "USD";
    // System.out.println(copyAmount);
    // System.out.println(currencyBoxes.get("UAH"));
    // }
}
