package com.kursyjava.atm;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ATMServices {
    private final static List<Integer> NOMINALS_LIST = Collections.unmodifiableList(Arrays.asList(1000, 500, 100, 50, 10, 5, 1));

    public static boolean isAdd(String inputLine) {
        // + <currency> <nominal> <count>
        String[] words = inputLine.split(" ");
        if (words.length != 4) {
            return false;
        }

        String operation = words[0];
        if (!operation.equals("+")) {
            return false;
        }

        String currencyCode = words[1];
        try {
            Currency.getInstance(currencyCode);
        } catch (IllegalArgumentException e) {
            return false;
        }

        // Valid values are 10n and 5*10n, 0<=n<=2
        String nominalText = words[2];
        int nominal = -1;
        try {
            nominal = Integer.parseInt(nominalText);
        } catch (NumberFormatException e) {
            return false;
        }
        if (!NOMINALS_LIST.contains(nominal)) {
            return false;
        }
        // <count> is any positive integer
        String countText = words[3];
        int count = -1;
        try {
            count = Integer.parseInt(countText);
        } catch (NumberFormatException e) {
            return false;
        }
        if (count < 1) {
            return false;
        }

        return true;
    }

    public static void add(String inputLine, SortedMap<String, CurrencyBox> currencyBoxes) {
        String[] words = inputLine.split(" ");
        String currencyCode = words[1];
        int nominal = Integer.parseInt(words[2]);
        int count = Integer.parseInt(words[3]);

        if (currencyBoxes.get(currencyCode) == null) {
            currencyBoxes.put(currencyCode, new CurrencyBox(currencyCode));
        }
        currencyBoxes.get(currencyCode).addMoney(nominal, count);
    }

    public static boolean isWithdraw(String inputLine, SortedMap<String, CurrencyBox> currencyBoxes) {
        // - <currency> <amount>
        String[] words = inputLine.split(" ");
        if (words.length != 3) {
            return false;
        }

        String operation = words[0];
        if (!operation.equals("-")) {
            return false;
        }

        String currencyCode = words[1];
        try {
            Currency.getInstance(currencyCode);
        } catch (IllegalArgumentException e) {
            return false;
        }

        // Valid values are 10n and 5*10n, 0<=n<=2
        String amountText = words[2];
        int amount = -1;
        try {
            amount = Integer.parseInt(amountText);
        } catch (NumberFormatException e) {
            return false;
        }
        if (currencyBoxes.get(currencyCode) == null) {
            return false;
        }
        int copyAmount = amount;
        for (int i = 0; i < NOMINALS_LIST.size(); i++) {
            int nominal = NOMINALS_LIST.get(i);
            int count = currencyBoxes.get(currencyCode).getNominalCount(nominal);
            for (int j = 0; j < count; j++) {
                if (copyAmount >= nominal) {
                    copyAmount = copyAmount - nominal;
                    // System.out.println(copyAmount);
                }
            }
        }
        if (copyAmount != 0) {
            return false;
        }
        return true;
    }

    public static boolean isAuthorized(PrintWriter out, String inputLine) throws ParserConfigurationException, SAXException, IOException {
        boolean isAuthorized;
        String[] words = inputLine.split(" ");
        if (words.length != 4) {
            isAuthorized = false;
            out.println("INVALID CREDENTIALS. BYE1");
            return isAuthorized;
        }

        String loginParameter = words[0];
        String login = words[1];
        String passwordParameter = words[2];
        String password = words[3];

        if (!loginParameter.equals("-l")) {
            isAuthorized = false;
            out.println("INVALID CREDENTIALS. BYE2");
            return isAuthorized;
        } else if (login == null) {
            isAuthorized = false;
            out.println("INVALID CREDENTIALS. BYE3");
            return isAuthorized;
        } else if (!passwordParameter.equals("-p")) {
            isAuthorized = false;
            out.println("INVALID CREDENTIALS. BYE4");
            return isAuthorized;
        } else if (password == null) {
            isAuthorized = false;
            out.println("INVALID CREDENTIALS. BYE5");
            return isAuthorized;
        } else {
            Map<String, String> userMap = XMLReader.getUsersMap();
            if (userMap.getOrDefault(login, null).equals(password)) {
                isAuthorized = true;
                out.println("WELCOME, " + login);
                return isAuthorized;
            } else {
                isAuthorized = false;
                out.println("INVALID CREDENTIALS. BYE6");
                return isAuthorized;
            }
        }
        // return isAuthorized;
    }

    public static void withdraw(String inputLine, PrintWriter out, SortedMap<String, CurrencyBox> currencyBoxes) {
        String[] words = inputLine.split(" ");
        String currencyCode = words[1];
        int amount = Integer.parseInt(words[2]);
        // -USD 500
        // 100 3
        // 10 20

        for (int i = 0; i < NOMINALS_LIST.size(); i++) {
            int nominal = NOMINALS_LIST.get(i);
            int count = currencyBoxes.get(currencyCode).getNominalCount(nominal);
            int k = 0;

            // 333
            // 100 * 2
            // 10 * 3
            // 1 * 3

            // 333 % 100 = 3
            // 333 - 3 * 100 = 33
            // 33 % 10 = 3
            // 33 - 10 * 3 = 3
            // 3 - 3 * 1 = 0

            for (int j = 0; j < count; j++) {
                if (amount >= nominal) {
                    amount = amount - nominal;
                    currencyBoxes.get(currencyCode).removeMoney(nominal);
                    k = k + 1;
                }
            }
            if (k != 0) {
                out.println(nominal + " " + k);
            }
        }
    }

}
