package com.kursyjava.sax;

import java.io.FileWriter;
import java.io.IOException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class TestWriter {

    public static void main(String[] args) {

        try {
            long start = System.currentTimeMillis();
            String fileName = "sax.xml";
            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xmlStreamWriter = null;
            xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(new FileWriter(fileName));

            OfferXmlWriter offerXmlWriter = new OfferXmlWriter(xmlStreamWriter);
            offerXmlWriter.writeStart();
            RandomOfferGenerator randomOfferGenerator = new RandomOfferGenerator();
            for (int i = 0; i < 5_000_000; i++) {
                offerXmlWriter.writeOffer(randomOfferGenerator.generateOffer());
            }
            offerXmlWriter.writeEnd();
            long end = System.currentTimeMillis();
            System.out.println("Done Generate XML = " + (end - start) + " ms");

        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }

    }
}