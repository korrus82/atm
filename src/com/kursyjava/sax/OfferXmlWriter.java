package com.kursyjava.sax;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class OfferXmlWriter {
    private XMLStreamWriter xmlStreamWriter;

    public OfferXmlWriter(XMLStreamWriter xmlStreamWriter) {
        this.xmlStreamWriter = xmlStreamWriter;
    }

    public void writeStart() {
        // start writing xml file
        try {
            xmlStreamWriter.writeStartDocument("UTF-8", "1.0");
            xmlStreamWriter.writeCharacters("\n");
            xmlStreamWriter.writeStartElement("Offers");
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void writeEnd() {
        // end writing xml file
        try {
            xmlStreamWriter.writeEndDocument();
            xmlStreamWriter.flush();
            xmlStreamWriter.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void writeOffer(Offer offer) {
        try {
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeStartElement("Offer");

            xmlStreamWriter.writeCharacters("\n\t\t");
            xmlStreamWriter.writeStartElement("Title");
            xmlStreamWriter.writeCData(offer.getTitle());
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("Description");
            xmlStreamWriter.writeCData(offer.getDescription());
            // xmlStreamWriter.writeCharacters(offer.getDescription());
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("Price");
            xmlStreamWriter.writeCharacters(String.valueOf(offer.getPrice()));
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("Weight");
            xmlStreamWriter.writeCharacters(String.valueOf(offer.getWeight()));
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeEndElement();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
