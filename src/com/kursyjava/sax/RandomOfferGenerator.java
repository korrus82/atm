package com.kursyjava.sax;

import java.util.Random;

public class RandomOfferGenerator {
    public Random random;

    public RandomOfferGenerator() {
        random = new Random();
    }

    public Offer generateOffer() {
        Offer offer = new Offer(generateRandomString(20), generateRandomString(30), random.nextInt(10000), random.nextInt(100));
        // System.out.println(offer);
        return offer;
    }

    public String generateRandomString(int length) {
        // StringBuilder stringBuilder = new StringBuilder(length);
        // for (int i = 0; i < length; i++) {
        // char tmp = (char) ('a' + random.nextInt('z' - 'a'));
        // stringBuilder.append(tmp);
        // }
        // return stringBuilder.toString();
        String string = "";
        for (int i = 0; i < length; i++) {
            char tmp = (char) ('a' + random.nextInt('z' - 'a'));
            string = string + tmp;
        }
        return string;
    }
}
