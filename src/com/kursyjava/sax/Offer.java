package com.kursyjava.sax;

public class Offer {
    private String title;
    private String description;
    private int price;
    private int weight;

    public Offer() {
    }

    public Offer(String title, String description, int price, int weight) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.weight = weight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Offer [title=" + title + ", description=" + description + ", price=" + price + ", weight=" + weight + "]";
    }

}
