package com.kursyjava.sax;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class OfferXmlReader {
    public XMLStreamReader xmlStreamReader;
    private int event;

    public OfferXmlReader(XMLStreamReader xmlStreamReader) {
        this.event = xmlStreamReader.getEventType();
        this.xmlStreamReader = xmlStreamReader;
    }

    public List<Offer> readxml() {

        boolean isTitle = false;
        boolean isDescription = false;
        boolean isPrice = false;
        boolean isWeight = false;
        List<Offer> list = new ArrayList<>();
        Offer offer = null;
        try {
            int event = xmlStreamReader.getEventType();
            while (true) {

                switch (event) {
                case XMLStreamConstants.START_DOCUMENT:
                    // System.out.println("Start Document.");
                    break;

                case XMLStreamConstants.START_ELEMENT:
                    if (xmlStreamReader.getLocalName().equals("Offer")) {
                        offer = new Offer();
                    } else if (xmlStreamReader.getLocalName().equals("Title")) {
                        isTitle = true;
                    } else if (xmlStreamReader.getLocalName().equals("Description")) {
                        isDescription = true;
                    } else if (xmlStreamReader.getLocalName().equals("Price")) {
                        isPrice = true;
                    } else if (xmlStreamReader.getLocalName().equals("Weight")) {
                        isWeight = true;
                    }

                    // System.out.println("Start Element: " + xmlStreamReader.getName());
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if (xmlStreamReader.isWhiteSpace())
                        break;
                    if (isTitle) {
                        offer.setTitle(xmlStreamReader.getText());
                        isTitle = false;
                    } else if (isDescription) {
                        offer.setDescription(xmlStreamReader.getText());
                        isDescription = false;
                    } else if (isPrice) {
                        offer.setPrice(Integer.parseInt(xmlStreamReader.getText()));
                        isPrice = false;
                    } else if (isWeight) {
                        offer.setWeight(Integer.parseInt(xmlStreamReader.getText()));
                        isWeight = false;
                    }
                    // System.out.println("Text: " + xmlStreamReader.getText());
                    break;
                case XMLStreamConstants.CDATA:
                    // if (isDescription) {
                    // offer.setDescription(xmlStreamReader.getText());
                    // isDescription = false;
                    // }
                    // System.out.println("CDATA: " + xmlStreamReader.getText());
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (xmlStreamReader.getLocalName().equals("Offer")) {
                        list.add(offer);
                    }
                    // System.out.println("End Element:" + xmlStreamReader.getName());
                    break;
                case XMLStreamConstants.END_DOCUMENT:
                    // System.out.println("End Document.");
                    break;
                }

                if (!xmlStreamReader.hasNext()) {

                    break;
                }

                event = xmlStreamReader.next();
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return list;

    }

    public Offer nextOffer() {
        // Класс reader должен иметь метод nextOffer который возвращает Offer, или null
        boolean isTitle = false;
        boolean isDescription = false;
        boolean isPrice = false;
        boolean isWeight = false;
        Offer offer = null;
        try {
            // event = xmlStreamReader.getEventType();
            while (true) {

                switch (event) {
                case XMLStreamConstants.START_DOCUMENT:
                    // System.out.println("Start Document.");
                    break;

                case XMLStreamConstants.START_ELEMENT:
                    if (xmlStreamReader.getLocalName().equals("Offer")) {
                        offer = new Offer();
                    } else if (xmlStreamReader.getLocalName().equals("Title")) {
                        isTitle = true;
                    } else if (xmlStreamReader.getLocalName().equals("Description")) {
                        isDescription = true;
                    } else if (xmlStreamReader.getLocalName().equals("Price")) {
                        isPrice = true;
                    } else if (xmlStreamReader.getLocalName().equals("Weight")) {
                        isWeight = true;
                    }

                    // System.out.println("Start Element: " + xmlStreamReader.getName());
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if (xmlStreamReader.isWhiteSpace())
                        break;
                    if (isTitle) {
                        offer.setTitle(xmlStreamReader.getText());
                        isTitle = false;
                    } else if (isDescription) {
                        offer.setDescription(xmlStreamReader.getText());
                        isDescription = false;
                    } else if (isPrice) {
                        offer.setPrice(Integer.parseInt(xmlStreamReader.getText()));
                        isPrice = false;
                    } else if (isWeight) {
                        offer.setWeight(Integer.parseInt(xmlStreamReader.getText()));
                        isWeight = false;
                    }
                    // System.out.println("Text: " + xmlStreamReader.getText());
                    break;
                case XMLStreamConstants.CDATA:
                    // if (isDescription) {
                    // offer.setDescription(xmlStreamReader.getText());
                    // isDescription = false;
                    // }
                    // System.out.println("CDATA: " + xmlStreamReader.getText());
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (xmlStreamReader.getLocalName().equals("Offer")) {
                        event = XMLStreamConstants.START_ELEMENT;
                        return offer;
                    }
                    // System.out.println("End Element:" + xmlStreamReader.getName());
                    break;
                case XMLStreamConstants.END_DOCUMENT:
                    // System.out.println("End Document.");
                    break;
                }

                if (!xmlStreamReader.hasNext()) {
                    offer = null;
                    return offer;
                    // break;
                }

                event = xmlStreamReader.next();
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        return offer;
    }
}
