package com.kursyjava.sax;

import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class TestReader {

    public static void main(String[] args) {
        try {
            long start = System.currentTimeMillis();
            String fileName = "sax.xml";

            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(new FileInputStream(fileName));

            OfferXmlReader offerXmlReader = new OfferXmlReader(xmlStreamReader);

            Offer offer = null;
            while ((offer = offerXmlReader.nextOffer()) != null) {
                System.out.println(offer);
            }
            System.out.println(offerXmlReader.nextOffer());
            long end = System.currentTimeMillis();
            System.out.println("Done Read XML = " + (end - start) + " ms");
            // System.out.println("Read from xml = " + list.size());

        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }

    }

}
